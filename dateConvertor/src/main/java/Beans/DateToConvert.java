package Beans;

import java.util.Calendar;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;


public class DateToConvert {
	
	private int id;
	private Calendar dateTC;
	
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	
	public Calendar getDateTC() {
		return dateTC;
	}
	
	public void setDateTC(Calendar dateTC) {
		this.dateTC = dateTC;
	}
	
}
