package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.GregorianCalendar;

import org.springframework.jdbc.core.RowMapper;

import Beans.DateToConvert;

public class DateConvertRowMapper implements RowMapper<DateToConvert>{

	
	public DateToConvert mapRow(ResultSet rs, int rowNu) throws SQLException {
		DateToConvert dtc = new DateToConvert();
		dtc.setId(rs.getInt("id"));
		Calendar cal = new GregorianCalendar();
		cal.setTime(rs.getTimestamp("date"));
		dtc.setDateTC(cal);
		return dtc;
	}

}