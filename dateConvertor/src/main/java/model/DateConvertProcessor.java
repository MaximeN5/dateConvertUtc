package model;

import org.springframework.batch.item.ItemProcessor;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

import Beans.DateToConvert;

public class DateConvertProcessor implements ItemProcessor<DateToConvert, DateToConvert>{

	public static final TimeZone utcTZ = TimeZone.getTimeZone("UTC");
	
	public DateToConvert process(DateToConvert date) throws Exception {
		Calendar date1 = date.getDateTC();
	  // SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
      // System.out.println(sdf.format(date1.getTime())+"\n");
        long utcTimeStamp = toUTC(date1.getTimeInMillis(), date1.getTimeZone());
        Calendar utcCal = Calendar.getInstance();
        utcCal.setTimeInMillis(utcTimeStamp);
      //  System.out.println("toUTC: "+sdf.format(utcCal.getTime())+"\n");
        date.setDateTC(utcCal);
		return date;
	}
	
	public static long toUTC(long time, TimeZone from) {
        return convertTime(time, from, utcTZ);
    }
 
    public static long convertTime(long time, TimeZone from, TimeZone to) {
        return time + getTimeZoneOffset(time, from, to);
    }
 
    private static long getTimeZoneOffset(long time, TimeZone from, TimeZone to) {
        int fromOffset = from.getOffset(time);
        int toOffset = to.getOffset(time);
        int diff = 0;
 
        if (fromOffset >= 0){
            if (toOffset > 0){
                toOffset = -1*toOffset;
            } else {
                toOffset = Math.abs(toOffset);
            }
            diff = (fromOffset+toOffset)*-1;
        } else {
            if (toOffset <= 0){
                toOffset = -1*Math.abs(toOffset);
            }
            diff = (Math.abs(fromOffset)+toOffset);
        }
        return diff;
    }
	
	
	
	
	

}
