package model;

import java.util.List;

import org.joda.time.DateTime;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;


public class DateConvertJobListener implements JobExecutionListener{
	
	private DateTime StartTime, stopTime;

	 @Override
	    public void beforeJob(JobExecution jobExecution) {
	        StartTime = new DateTime();
	        System.out.println("ExamResult Job starts at :"+StartTime);
	    }
	 
	    @Override
	    public void afterJob(JobExecution jobExecution) {
	        stopTime = new DateTime();
	        System.out.println("ConversionResult Job stops at :"+stopTime);
	        System.out.println("Total time take in millis :"+getTimeInMillis(StartTime , stopTime));
	 
	        if(jobExecution.getStatus() == BatchStatus.COMPLETED){
	            System.out.println("ConversionResult job completed successfully");
	            //Here you can perform some other business logic like cleanup
	        }else if(jobExecution.getStatus() == BatchStatus.FAILED){
	            System.out.println("ConversionResult job failed with following exceptions ");
	            List<Throwable> exceptionList = jobExecution.getAllFailureExceptions();
	            for(Throwable th : exceptionList){
	                System.err.println("exception :" +th.getLocalizedMessage());
	            }
	        }
	    }
	 
	    private long getTimeInMillis(DateTime start, DateTime stop){
	        return stop.getMillis() - start.getMillis();
	    }


	
	
	

}
