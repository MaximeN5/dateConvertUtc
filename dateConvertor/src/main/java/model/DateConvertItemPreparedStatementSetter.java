package model;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.util.Calendar;

import org.springframework.batch.item.database.ItemPreparedStatementSetter;

import Beans.DateToConvert;

public class DateConvertItemPreparedStatementSetter implements ItemPreparedStatementSetter<DateToConvert> {

	@Override
	public void setValues(DateToConvert result, PreparedStatement ps) throws SQLException {
		ps.setInt(2, result.getId());
	    Timestamp time = new Timestamp(result.getDateTC().getTimeInMillis());
		ps.setTimestamp(1, time);
	}

}
